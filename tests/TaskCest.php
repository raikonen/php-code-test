<?php

declare(strict_types=1);

namespace Tests;

use Codeception\Example;
use Exception;
use Mockery;
use Mockery\MockInterface;
use Tymeshift\PhpTest\Components\HttpClientInterface;
use Tymeshift\PhpTest\Domains\Task\TaskCollection;
use Tymeshift\PhpTest\Domains\Task\TaskEntityInterface;
use Tymeshift\PhpTest\Domains\Task\TaskFactory;
use Tymeshift\PhpTest\Domains\Task\TaskRepository;
use Tymeshift\PhpTest\Domains\Task\TaskStorage;
use UnitTester;

class TaskCest
{
    private ?TaskRepository $taskRepository;
    private MockInterface $httpClientMock;

    public function _before()
    {
        $this->httpClientMock = Mockery::mock(HttpClientInterface::class);

        $this->taskRepository = new TaskRepository(
            new TaskStorage($this->httpClientMock),
            new TaskFactory()
        );
    }

    public function _after()
    {
        $this->taskRepository = null;
        Mockery::close();
    }

    /**
     * @dataProvider tasksDataProvider
     */
    public function testGetTasks(Example $example, UnitTester $tester)
    {
        [
            $task1,
            $task2,
            $task3,
        ] = $example;

        $this->httpClientMock
            ->shouldReceive("request")
            ->with(
                "GET",
                "https://www.tymeshift.local/api/v1/schedules/1/tasks"
            )
            ->andReturn([
                $task1,
                $task2,
                $task3,
            ]);

        $tasks = $this->taskRepository
            ->getByScheduleId(1);

        $tester->assertInstanceOf(TaskCollection::class, $tasks);
        $tester->assertSame($example->count(), $tasks->count());

        /** @var TaskEntityInterface[] $tasks */
        foreach ($tasks as $task) {
            $tester->assertInstanceOf(TaskEntityInterface::class, $task);
        }

        $this->assertObjectHasData($tester, $task1, $tasks[0]);
        $this->assertObjectHasData($tester, $task2, $tasks[1]);
        $this->assertObjectHasData($tester, $task3, $tasks[2]);
    }

    public function testGetTasksFailed(UnitTester $tester)
    {
        $this->httpClientMock
            ->shouldReceive("request")
            ->once()
            ->with(
                "GET",
                "https://www.tymeshift.local/api/v1/schedules/4/tasks"
            )
            ->andReturn([]);

        $tester->expectThrowable(
            Exception::class,
            function () {
                $this->taskRepository->getByScheduleId(4);
            }
        );
    }

    public function tasksDataProvider()
    {
        return [
            [
                [
                    "id" => 123,
                    "schedule_id" => 1,
                    "start_time" => 0,
                    "duration" => 3600,
                ],
                [
                    "id" => 431,
                    "schedule_id" => 1,
                    "start_time" => 3600,
                    "duration" => 650,
                ],
                [
                    "id" => 332,
                    "schedule_id" => 1,
                    "start_time" => 5600,
                    "duration" => 3600,
                ],
            ],
        ];
    }


    private function assertObjectHasData(
        UnitTester $tester,
        array $task_data,
        TaskEntityInterface $task
    ): void
    {
        $tester->assertEquals($task_data['id'], $task->getId());
        $tester->assertEquals($task_data['schedule_id'], $task->getScheduleId());
        $tester->assertEquals($task_data['start_time'], $task->getStartTime()->getTimestamp());
        $tester->assertEquals($task_data['duration'], $task->getDuration());
    }
}
