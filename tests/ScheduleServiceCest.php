<?php

declare(strict_types=1);

use Mockery\MockInterface;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleEntity;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleItemFactoryInterface;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleRepository;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleService;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleTaskItem;
use Tymeshift\PhpTest\Exceptions\StorageDataMissingException;

class ScheduleServiceCest
{
    private ScheduleService $scheduleService;
    private MockInterface $scheduleRepository;
    private MockInterface $scheduleItemFactory;

    public function _before()
    {
        $this->scheduleRepository = Mockery::mock(ScheduleRepository::class);
        $this->scheduleItemFactory = Mockery::mock(ScheduleItemFactoryInterface::class);

        $this->scheduleService = new ScheduleService(
            $this->scheduleRepository,
            $this->scheduleItemFactory
        );
    }

    public function _after()
    {
        Mockery::close();
    }

    public function testWillThrowExceptionIfCanFindSchedule(UnitTester $tester)
    {
        $this->scheduleRepository
            ->shouldReceive('getById')
            ->with(4)
            ->andThrowExceptions([new StorageDataMissingException]);

        $tester->expectThrowable(
            Exception::class,
            function () {
                $this->scheduleService->getById(4);
            }
        );
    }

    public function testWillReturnScheduleWithItems(UnitTester $tester)
    {
        $startTime = (new DateTime())->setTimestamp( 1631232000);
        $endTime = (new DateTime())->setTimestamp( 1631232000 + 86400);
        $schedule = new ScheduleEntity();

        $tester->assertEmpty($schedule->getItems());

        $this->scheduleRepository
            ->shouldReceive('getById')
            ->with(4)
            ->andReturn($schedule);

        $this->scheduleItemFactory
            ->shouldReceive('createTaskItemsForSchedule')
            ->with(4)
            ->andReturn([
                new ScheduleTaskItem(
                    4,
                    $startTime,
                    $endTime
                ),
                new ScheduleTaskItem(
                    4,
                    $startTime,
                    $endTime
                ),
            ]);

        $schedule = $this->scheduleService->getById(4);

        $tester->assertInstanceOf(ScheduleEntity::class, $schedule);

        $tester->assertCount(2, $schedule->getItems());
    }
}