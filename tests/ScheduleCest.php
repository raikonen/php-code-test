<?php

declare(strict_types=1);

namespace Tests;

use Codeception\Example;
use Mockery;
use Mockery\MockInterface;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleRepository;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleFactory;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleStorage;
use Tymeshift\PhpTest\Domains\Schedule\ScheduleStorageInterface;
use Tymeshift\PhpTest\Exceptions\StorageDataMissingException;
use UnitTester;

class ScheduleCest
{
    private ?MockInterface $scheduleStorageMock;
    private ?ScheduleRepository $scheduleRepository;
    
    public function _before()
    {
        $this->scheduleStorageMock = Mockery::mock(ScheduleStorageInterface::class);
        $this->scheduleRepository = new ScheduleRepository(
            $this->scheduleStorageMock,
            new ScheduleFactory()
        );
    }

    public function _after()
    {
        $this->scheduleRepository = null;
        $this->scheduleStorageMock = null;

        Mockery::close();
    }

    /**
     * @dataProvider scheduleProvider
     */
    public function testGetByIdSuccess(Example $example, UnitTester $tester)
    {
        [
            'id' => $id,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'name' => $name,
        ] = $example;

        $this->scheduleStorageMock
            ->shouldReceive('getById')
            ->with($id)
            ->andReturn([
                'id' => $id,
                'start_time' => $startTime,
                'end_time' => $endTime,
                'name' => $name,
            ]);

        $entity = $this->scheduleRepository->getById($id);

        $tester->assertEquals($id, $entity->getId());

        $tester->assertEquals(
            $startTime,
            $entity->getStartTime()->getTimestamp()
        );
        $tester->assertEquals(
            $endTime,
            $entity->getEndTime()->getTimestamp()
        );
    }

    public function testGetByIdFail(UnitTester $tester)
    {
        $this->scheduleStorageMock
            ->shouldReceive('getById')
            ->with(4)
            ->andReturn([]);

        $tester->expectThrowable(
            StorageDataMissingException::class,
            function () {
                $this->scheduleRepository->getById(4);
            }
        );
    }

    protected function scheduleProvider(): array
    {
        return [
            [
                'id' => 1,
                'start_time' => 1631232000,
                'end_time' => 1631232000 + 86400,
                'name' => 'Test',
            ],
            [
                'id' => 12,
                'start_time' => 1631232202,
                'end_time' => 1631232202 + 86400,
                'name' => 'Test 12',
            ],
            [
                'id' => 14,
                'start_time' => 1631232055,
                'end_time' => 1631232202 + 86400,
                'name' => 'Test 14',
            ],
        ];
    }
}