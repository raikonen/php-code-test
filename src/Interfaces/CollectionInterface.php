<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Interfaces;

interface CollectionInterface
{
    public function add(EntityInterface $entity): self;
    public function createFromArray(array $data, FactoryInterface $factory): self;
    public function toArray(): array;
}
