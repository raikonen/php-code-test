<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Exceptions;

use Exception;

class StorageDataMissingException extends Exception
{
}
