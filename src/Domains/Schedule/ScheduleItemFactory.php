<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

use DateTime;
use Tymeshift\PhpTest\Domains\Task\TaskEntityInterface;
use Tymeshift\PhpTest\Domains\Task\TaskRepository;

class ScheduleItemFactory implements ScheduleItemFactoryInterface
{
    private TaskRepository $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function createTaskItemsForSchedule(int $id): array
    {
        $items = [];
        $tasks = $this->taskRepository->getByScheduleId($id);

        if (!$tasks) {
            return $items;
        }

        /** @var TaskEntityInterface[] $tasks */
        foreach ($tasks as $task) {
            $items[] = new ScheduleTaskItem(
                $task->getScheduleId(),
                $task->getStartTime(),
                $this->getEndTime($task)
            );
        }

        return $items;
    }

    private function getEndTime(TaskEntityInterface $task): DateTime
    {
        return (new DateTime())->setTimestamp($task->getStartTime()->getTimestamp() + $task->getDuration());
    }
}
