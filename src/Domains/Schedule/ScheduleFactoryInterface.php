<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

use Tymeshift\PhpTest\Interfaces\EntityInterface;
use Tymeshift\PhpTest\Interfaces\FactoryInterface;

interface ScheduleFactoryInterface extends FactoryInterface
{
    public function createEntity(array $data): EntityInterface;
}
