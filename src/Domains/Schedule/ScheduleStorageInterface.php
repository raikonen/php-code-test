<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

interface ScheduleStorageInterface
{
    public function getById(int $id): array;
    public function getByIds(array $ids): array;
}
