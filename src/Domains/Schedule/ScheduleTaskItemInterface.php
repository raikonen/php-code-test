<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

interface ScheduleTaskItemInterface extends ScheduleItemInterface
{
    const TYPE = "Task";
}
