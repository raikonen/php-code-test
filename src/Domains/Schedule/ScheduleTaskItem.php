<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

class ScheduleTaskItem extends ScheduleItemEntity implements ScheduleTaskItemInterface
{
    public function getType(): string
    {
        return ScheduleTaskItemInterface::TYPE;
    }
}
