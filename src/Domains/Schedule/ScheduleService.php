<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

use Exception;
use Tymeshift\PhpTest\Exceptions\StorageDataMissingException;

class ScheduleService implements ScheduleServiceInterface
{
    private ScheduleRepository $repository;
    private ScheduleItemFactoryInterface $factory;

    public function __construct(
        ScheduleRepository $repository,
        ScheduleItemFactoryInterface $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function getById(int $id): ScheduleEntityInterface
    {
        try {
            $schedule = $this->repository->getById($id);
        } catch (StorageDataMissingException $exception) {
            throw new Exception("Schedule #{$id} not found.");
        }

        $items = $this->factory->createTaskItemsForSchedule($id);

        if (!$items) {
            return $schedule;
        }

        return $schedule->addItems(...$items);
    }
}