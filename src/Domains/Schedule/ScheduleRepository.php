<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

use Tymeshift\PhpTest\Exceptions\StorageDataMissingException;
use Tymeshift\PhpTest\Interfaces\EntityInterface;

class ScheduleRepository
{
    private ScheduleStorageInterface $storage;
    private ScheduleFactoryInterface $factory;

    public function __construct(ScheduleStorageInterface $storage, ScheduleFactoryInterface $factory)
    {
        $this->storage = $storage;
        $this->factory = $factory;
    }

    /**
     * @throws StorageDataMissingException
     */
    public function getById(int $id): EntityInterface
    {
        $data =  $this->storage->getById($id);

        if (empty($data)) {
            return throw new StorageDataMissingException();
        }

        return $this->factory
            ->createEntity(
                $this->storage->getById($id)
            );
    }
}