<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

use DateTime;
use Tymeshift\PhpTest\Interfaces\EntityInterface;

interface ScheduleEntityInterface extends EntityInterface
{
    public function getId(): int;
    public function setId(int $id): ScheduleEntity;
    public function getName(): string;
    public function setName(string $name): ScheduleEntity;
    public function getStartTime(): DateTime;
    public function setStartTime(DateTime $startTime): ScheduleEntity;
    public function getEndTime(): DateTime;
    public function setEndTime(DateTime $endTime): ScheduleEntity;
    public function addItems(ScheduleItemInterface ...$scheduleItems): ScheduleEntityInterface;
    /**
     * @return ScheduleItemInterface[]
     */
    public function getItems(): array;
}
