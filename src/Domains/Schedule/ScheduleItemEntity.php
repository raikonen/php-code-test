<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

use DateTimeInterface;

abstract class ScheduleItemEntity implements ScheduleItemInterface
{
    private int $scheduleId;
    private DateTimeInterface $startTime;
    private DateTimeInterface $endTime;

    public function __construct(
        int $scheduleId,
        DateTimeInterface $startTime,
        DateTimeInterface $endTime
    ) {
        $this->scheduleId = $scheduleId;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
    }

    public function getScheduleId(): int
    {
        return $this->scheduleId;
    }

    public function getStartTime(): int
    {
        return $this->startTime->getTimestamp();
    }

    public function getEndTime(): int
    {
        return $this->endTime->getTimestamp();
    }
}
