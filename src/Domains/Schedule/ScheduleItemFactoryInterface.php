<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Schedule;

interface ScheduleItemFactoryInterface
{
    public function createTaskItemsForSchedule(int $id): array;
}
