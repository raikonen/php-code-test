<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Task;

use Tymeshift\PhpTest\Components\HttpClientInterface;

class TaskStorage implements TaskStorageInterface
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->client = $httpClient;
    }

    public function getByScheduleId(int $id): array
    {
        return $this->client->request(
            "GET",
            "https://www.tymeshift.local/api/v1/schedules/{$id}/tasks"
        );
    }

    public function getByIds(array $ids): array
    {
        $ids = implode(',', $ids);

        return $this->client->request(
            "GET",
            "https://www.tymeshift.local/api/v1/tasks?ids={$ids}"
        );
    }

    public function getById(int $id): array
    {
        return $this->client->request(
            "GET",
            "https://www.tymeshift.local/api/v1/tasks/{$id}"
        );
    }
}
