<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Task;

use DateTimeInterface;
use JsonSerializable;
use Tymeshift\PhpTest\Interfaces\EntityInterface;

interface TaskEntityInterface extends EntityInterface, JsonSerializable
{
    public function getId(): int;
    public function setId(int $id): void;
    public function getScheduleId(): int;
    public function setScheduleId(int $schedule_id): void;
    public function getStartTime(): DateTimeInterface;
    public function setStartTime(DateTimeInterface $start_time): void;
    public function getDuration(): int;
    public function setDuration(int $duration): void;
}
