<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Task;

use DateTimeInterface;

class TaskEntity implements TaskEntityInterface
{
    private int $id;
    private int $schedule_id;
    private DateTimeInterface $start_time;
    private int $duration;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getScheduleId(): int
    {
        return $this->schedule_id;
    }

    public function setScheduleId(int $schedule_id): void
    {
        $this->schedule_id = $schedule_id;
    }

    public function getStartTime(): DateTimeInterface
    {
        return $this->start_time;
    }

    public function setStartTime(DateTimeInterface $start_time): void
    {
        $this->start_time = $start_time;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "schedule_id" => $this->getScheduleId(),
            "start_time" => $this->getStartTime(),
            "duration" => $this->getDuration(),
        ];
    }
}
