<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Task;

use Exception;
use Tymeshift\PhpTest\Interfaces\CollectionInterface;
use Tymeshift\PhpTest\Interfaces\FactoryInterface;

interface TaskFactoryInterface extends FactoryInterface
{
    /**
     * @throws Exception
     */
    public function createCollection(array $data): CollectionInterface;
}