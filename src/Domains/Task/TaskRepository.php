<?php

declare(strict_types=1);

namespace Tymeshift\PhpTest\Domains\Task;

use Tymeshift\PhpTest\Exceptions\InvalidCollectionDataProvidedException;
use Tymeshift\PhpTest\Exceptions\StorageDataMissingException;
use Tymeshift\PhpTest\Interfaces\EntityInterface;
use Tymeshift\PhpTest\Interfaces\RepositoryInterface;

class TaskRepository implements RepositoryInterface
{
    private TaskFactoryInterface $factory;
    private TaskStorageInterface $storage;

    public function __construct(
        TaskStorageInterface $storage,
        TaskFactoryInterface $factory
    ) {
        $this->factory = $factory;
        $this->storage = $storage;
    }

    public function getById(int $id): EntityInterface
    {
        $data = $this->storage->getById($id);

        if (empty($data)) {
            return throw new StorageDataMissingException();
        }

        return $this->factory
            ->createEntity(
                $data
            );
    }

    /**
     * @throws InvalidCollectionDataProvidedException
     * @throws StorageDataMissingException
     */
    public function getByScheduleId(int $scheduleId): TaskCollection
    {
        $data = $this->storage->getByScheduleId($scheduleId);

        if (empty($data)) {
            return throw new StorageDataMissingException();
        }

        return $this->factory
            ->createCollection(
                $data
            );
    }

    /**
     * @throws InvalidCollectionDataProvidedException
     */
    public function getByIds(array $ids): TaskCollection
    {
        return $this->factory
            ->createCollection(
                $this->storage->getByIds($ids)
            );
    }
}