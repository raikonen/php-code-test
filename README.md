# tymeshift's PHP code test 🧪

## Task Summary

- Fix code style
- Extract Interface for classes (code relies on Abstraction)
- Fix tests and implement features 
- Implement `ScheduleService` and test for service